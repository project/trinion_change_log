<?php

namespace Drupal\trinion_change_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Форма настроек для модуля Trinion_change_log
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_change_log_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trinion_change_log.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = self::getNodeTypes();
    $bundles = $this->config('trinion_change_log.settings')->get('bundles');
    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => 'Типы материалов, изменения которых будут логироваться',
      '#options' => $options,
      '#default_value' => $bundles ? $bundles : [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('trinion_change_log.settings')
      ->set('bundles', $form_state->getValue('bundles'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  public static function getNodeTypes() {
    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    $items = [];
    foreach ($types as $key => $type) {
      if ($key != 'change_log' && $key != 'stroka_istorii_izmeneniy')
        $items[$key] = $type->label();
    }
    return $items;
  }

}
