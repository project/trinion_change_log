<?php

namespace Drupal\trinion_change_log\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a Trinion changelog form.
 */
class StatusZadachOtchetForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_change_log_status_zadach_otchet';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#action'] = '/istoriya_izmeneniy/status';
    $form['date'] = [
      '#type' => 'date',
      '#title' => t('Date'),
      '#default_value' => $this->selectedDate(),
      '#required' => TRUE,
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Proceed'),
    ];

    if ($status = \Drupal::request()->get('status')) {
      $data = $this->getDataByStatus($status);
      $form['table'] = [
        '#theme' => 'table',
        '#header' => [t('Status'), t('Task'), t('Task subject'), t('Responsible'), t('Change autor')],
        '#rows' => $data,
      ];
    }
    else {
      $data = $this->getData();
      $form['table'] = [
        '#theme' => 'table',
        '#header' => [t('Status'), t('Quantity')],
        '#rows' => $data,
        '#empty' => 'List is empty',
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  public function selectedDate() {
    $date = \Drupal::request()->get('date');
    if (empty($date))
      $date = date('Y-m-d');
    return $date;
  }

  public function getData() {
    $date = $this->selectedDate();
    $query = \Drupal::database()->select('node__field_tcl_pole', 'p')
      ->condition('p.field_tcl_pole_target_id', 'node.zadacha.field_tz_status_zadachi');
    $query->join('node__field_tcl_novoe_znachenie', 'nz', 'nz.entity_id = p.entity_id');
    $query->addField('nz', 'field_tcl_novoe_znachenie_value');
    $query->join('node__field_tcl_izmeneniya', 'iz', 'iz.field_tcl_izmeneniya_target_id = p.entity_id');
    $query->join('node__field_tcl_object', 'to', 'to.entity_id = iz.entity_id');
    $query->condition('to.bundle', 'change_log');
    $query->join('node_field_data', 'n', 'n.nid = iz.entity_id');
    $query->condition('n.created', [strtotime($date), strtotime('tomorrow', strtotime($date))], 'BETWEEN');
    $query->addField('to', 'field_tcl_object_target_id');
    $date = $this->selectedDate();
    $cnts = [];
    $cnts_status = [];
    foreach ($query->execute()->fetchAll() as $row) {
      $row = (array)$row;
      if (isset($cnts[$row['field_tcl_novoe_znachenie_value']][$row['field_tcl_object_target_id']])) {
        continue;
      }
      $cnts[$row['field_tcl_novoe_znachenie_value']][$row['field_tcl_object_target_id']] = 1;
      if (!isset($cnts_status[$row['field_tcl_novoe_znachenie_value']]))
        $cnts_status[$row['field_tcl_novoe_znachenie_value']] = 0;
      $cnts_status[$row['field_tcl_novoe_znachenie_value']]++;
      $row['cnt'] = ['data' => ['#markup' => "<a href='/istoriya_izmeneniy/status/?status={$row['field_tcl_novoe_znachenie_value']}&date={$date}'>{$cnts_status[$row['field_tcl_novoe_znachenie_value']]}</a>"]];
      unset($row['field_tcl_object_target_id']);
    }
    $data = [];
    foreach ($cnts_status as $status => $cnt) {
      $data[] = [
        $status,
        ['data' => ['#markup' => "<a href='/istoriya_izmeneniy/status/?status={$status}&date={$date}'>{$cnt}</a>"]],
      ];
    }
    return $data;
  }

  public function getDataByStatus($status) {
    $date = $this->selectedDate();
    $query = \Drupal::database()->select('node__field_tcl_novoe_znachenie', 'nz')
      ->condition('nz.field_tcl_novoe_znachenie_value', $status);
    $query->join('node__field_tcl_izmeneniya', 'iz', 'iz.field_tcl_izmeneniya_target_id = nz.entity_id');
    $query->join('node__field_tcl_object', 'to', 'to.entity_id = iz.entity_id');
    $query->join('node__field_tz_tema', 'tt', 'tt.entity_id = to.field_tcl_object_target_id');
    $query->join('node__field_tz_otvetstvennyy', 'tr', 'tr.entity_id = to.field_tcl_object_target_id');
    $query->join('user__field_tb_nick_name', 'un', 'un.entity_id = tr.field_tz_otvetstvennyy_target_id');
    $query->join('node_field_data', 'n', 'n.nid = tt.entity_id');
    $query->join('node_field_data', 'niz', 'niz.nid = iz.entity_id');
    $query->join('user__field_tb_nick_name', 'un_change_author', 'un_change_author.entity_id = niz.uid');
    $query->condition('niz.created', [strtotime($date), strtotime('tomorrow', strtotime($date))], 'BETWEEN');
    $query->addField('to', 'field_tcl_object_target_id');
    $query->addField('tt', 'field_tz_tema_value');
    $query->addField('n', 'title');
    $query->addField('un_change_author', 'field_tb_nick_name_value', 'change_author');
    $query->addField('un', 'field_tb_nick_name_value');
    $query->groupBy('to.field_tcl_object_target_id');
    foreach ($query->execute()->fetchAll() as $row) {
      $line = (array)$row;
      $row = [
        $status,
        $line['title'],
        ['data' => ['#markup' => "<a href='/node/{$line['field_tcl_object_target_id']}'>{$line['field_tz_tema_value']}</a>"]],
        $line['field_tb_nick_name_value'],
        $line['change_author'],
      ];
      $data[] = $row;
    }

    return $data;
  }
}
