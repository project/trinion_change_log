<?php

namespace Drupal\trinion_change_log\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\Routing\Route;

/**
 * Проверка доступа к отчету по статусам задач
 */
class StatusZadachOtchetChecker implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(Route $route) {
    return AccessResult::allowed();
  }
}
