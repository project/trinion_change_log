<?php

namespace Drupal\trinion_change_log\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Izmeneniya' formatter.
 *
 * @FieldFormatter(
 *   id = "trinion_change_log_izmeneniya",
 *   label = @Translation("Название поля в ссылке на поле"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class IzmeneniyaFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element[0] = [
      '#markup' => is_null($items->first()->entity) ? '' : $items->first()->entity->label(),
    ];
    return $element;
  }

}
